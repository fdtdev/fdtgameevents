﻿using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.GameEvents
{
    public abstract class GameEvent1<T0> : GameEventBase
    {
        private readonly List<IGameEvent1Register<T0>> eventListeners = new List<IGameEvent1Register<T0>>();
        [Space] public T0 arg0;
        
#if UNITY_EDITOR
        public override void ResetData()
        {
            base.ResetData();
            eventListeners.Clear();
        }    
#endif
        public override void Raise()
        {
            Raise(arg0);
        }
        public void Raise(T0 arg0)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> Raise called for <color=green><b>{this.name}</b></color> with parameter <color=#add8e6ff>{arg0}</color>", this);
            }
            for(int _idx = eventListeners.Count -1; _idx >= 0; _idx--)
                eventListeners[_idx].OnEventRaised(this, arg0);
        }
        public void RegisterListener(IGameEvent1Register<T0> listener)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> RegisterListener called for <color=green><b>{this.name}</b></color> with listener {(listener is Object?((Object) listener).name:listener as object)}", this);
            }
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }


        public virtual string arg0label
        {
            get { return null; }
        }

        public List<IGameEvent1Register<T0>> GetEventListeners()
        {
            return eventListeners;
        }

        public void UnregisterListener(IGameEvent1Register<T0> listener)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> UnregisterListener called for <color=green><b>{this.name}</b></color> with listener {(listener is Object?((Object) listener).name:listener as object)}", this);
            }
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }
    }
}