﻿using UnityEngine;
using System;
using com.FDT.Common;

namespace com.FDT.GameEvents
{
	[System.Serializable]
	public class GameEventHandle: GameEventHandleBase, IGameEventRegister 
    {
#if UNITY_EDITOR
        public Action GetExecute()
        {
            return _Execute;
        }

        public GameObject GetGameObject()
        {
            return null;
        }
#endif
		[SerializeField] protected GameEvent _Event;

		protected Action _Execute;

        public void OnEventRaised(GameEvent evt)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_Execute != null)
                    _Execute();
            }
        }
        
		public void AddEventListener(Action action)
		{
            if (RuntimeApplication.isPlaying)
            {
                if (listeners == 0)
                    Init();
                _Execute += action;
                listeners++;
            }
        }
        public void RemoveEventListener(Action action)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (CanRemoveListener())
                {
                    _Execute -= action;
                }
            }
        }

        protected bool CanRemoveListener()
        {
            if (RuntimeApplication.isPlaying)
            {
                if (listeners > 0)
                {
                    listeners--;
                    if (listeners == 0)
                        Dispose();
                }

                return true;
            }
            else
            {
                listeners = 0;
                _Execute = null;
                return false;
            }
        }
        protected void Init()
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_Event != null)
                {
                    _Event.RegisterListener(this);
                }
            }
        }

		protected void Dispose()
		{
            if (RuntimeApplication.isPlaying)
            {
                if (_Event != null)
                {
                    _Event.UnregisterListener(this);
                }
            }
        }
        public GameEvent Event
        {
            set
            {
                Dispose();
                _Event = value;
                Init();
            }
        }
    }
}