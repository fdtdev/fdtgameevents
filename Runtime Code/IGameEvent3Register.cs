﻿using UnityEngine;

namespace com.FDT.GameEvents
{
	public interface IGameEvent3Register<T0, T1, T2>
	{
		void OnEventRaised (GameEvent3<T0, T1, T2> evt, T0 arg0, T1 arg1, T2 arg2);
		#if UNITY_EDITOR
		System.Action<T0, T1, T2> GetExecute();
		GameObject GetGameObject();
		#endif
	}
}