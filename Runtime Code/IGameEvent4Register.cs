﻿using UnityEngine;

namespace com.FDT.GameEvents
{
	public interface IGameEvent4Register<T0, T1, T2, T3>
	{
		void OnEventRaised (GameEvent4<T0, T1, T2, T3> evt, T0 arg0, T1 arg1, T2 arg2, T3 arg3);
		#if UNITY_EDITOR
		System.Action<T0, T1, T2, T3> GetExecute();
		GameObject GetGameObject();
		#endif
	}
}