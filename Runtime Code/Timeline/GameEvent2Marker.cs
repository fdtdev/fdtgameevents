﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace com.FDT.GameEvents
{
    public abstract class GameEvent2Marker<TEvt, T, T2> : Marker, INotification, INotificationOptionProvider, IGameEventSignal where TEvt:GameEvent2<T, T2>
    {
        [SerializeField] bool m_Retroactive;
        [SerializeField] bool m_EmitOnce;

        [SerializeField] protected TEvt _evt;


        public TEvt Evt
        {
            get { return _evt; }
        }

        [SerializeField] protected T _param;
        [SerializeField] protected T2 _param2;
        
        public T param
        {
            get { return _param; }
        }
        public T2 param2
        {
            get { return _param2; }
        }
        /// <summary>
        /// Use retroactive to emit the signal if playback starts after the SignalEmitter time.
        /// </summary>
        public bool retroactive
        {
            get { return m_Retroactive; }
            set { m_Retroactive = value; }
        }

        /// <summary>
        /// Use emitOnce to emit this signal once during loops.
        /// </summary>
        public bool emitOnce
        {
            get { return m_EmitOnce; }
            set { m_EmitOnce = value; }
        }
        PropertyName INotification.id
        {
            get
            {
                if (_evt != null)
                {
                    return new PropertyName(_evt.name);
                }
                return new PropertyName(string.Empty);
            }
        }
        NotificationFlags INotificationOptionProvider.flags
        {
            get
            {
                return (retroactive ? NotificationFlags.Retroactive : default(NotificationFlags)) |
                       (emitOnce ? NotificationFlags.TriggerOnce : default(NotificationFlags)) |
                       NotificationFlags.TriggerInEditMode;
            }
        }

        public void RaiseEvent()
        {
            _evt.Raise(_param, _param2);
        }
    }
}