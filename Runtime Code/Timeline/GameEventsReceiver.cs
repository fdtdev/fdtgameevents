﻿using UnityEngine;
using UnityEngine.Playables;

namespace com.FDT.GameEvents
{
    public class GameEventsReceiver : MonoBehaviour, INotificationReceiver
    {
        public void OnNotify(Playable origin, INotification notification, object context)
        {
            if (notification is IGameEventSignal)
            {
                (notification as IGameEventSignal).RaiseEvent();
            }
        }
    }
}