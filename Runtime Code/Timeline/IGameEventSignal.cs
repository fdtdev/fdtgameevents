﻿public interface IGameEventSignal
{
    void RaiseEvent();
}
