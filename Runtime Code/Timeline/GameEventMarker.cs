﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace com.FDT.GameEvents
{
    public class GameEventMarker : Marker, INotification, INotificationOptionProvider, IGameEventSignal
    {
        [SerializeField] bool m_Retroactive;
        [SerializeField] bool m_EmitOnce;

        [SerializeField] protected GameEvent _evt;

        public GameEvent Evt
        {
            get { return _evt; }
        }
        
        /// <summary>
        /// Use retroactive to emit the signal if playback starts after the SignalEmitter time.
        /// </summary>
        public bool retroactive
        {
            get { return m_Retroactive; }
            set { m_Retroactive = value; }
        }

        /// <summary>
        /// Use emitOnce to emit this signal once during loops.
        /// </summary>
        public bool emitOnce
        {
            get { return m_EmitOnce; }
            set { m_EmitOnce = value; }
        }
        PropertyName INotification.id
        {
            get
            {
                if (_evt != null)
                {
                    return new PropertyName(_evt.name);
                }
                return new PropertyName(string.Empty);
            }
        }
        NotificationFlags INotificationOptionProvider.flags
        {
            get
            {
                return (retroactive ? NotificationFlags.Retroactive : default(NotificationFlags)) |
                       (emitOnce ? NotificationFlags.TriggerOnce : default(NotificationFlags)) |
                       NotificationFlags.TriggerInEditMode;
            }
        }

        public void RaiseEvent()
        {
            _evt.Raise();
        }
    }
}