﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace com.FDT.GameEvents
{
    public class GameEventListenerBase: MonoBehaviour
    {
        [TextArea] public string description;
        public GameObject GetGameObject()
        {
            return gameObject;
        }
    }
}