﻿using UnityEngine;

namespace com.FDT.GameEvents
{
	public interface IGameEvent1Register<T0>
	{
		void OnEventRaised (GameEvent1<T0> evt, T0 arg0);
		#if UNITY_EDITOR
		System.Action<T0> GetExecute();
		GameObject GetGameObject();
		#endif
	}
}