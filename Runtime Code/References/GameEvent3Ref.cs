﻿using System;
using System.Collections.Generic;
using com.FDT.Common;
using com.FDT.GameEvents;
using UnityEngine;

namespace com.FDT.GameEvents
{
    [System.Serializable]
    public abstract class GameEvent3Ref<TEvt, T0, T1, T2> : GameEventRefBase, IGameEvent, IEquatable<TEvt> where TEvt : GameEvent3<T0, T1, T2>
    {
        [SerializeField] protected TEvt _evt;

        public void Raise()
        {
            if (RuntimeApplication.isPlaying)
            {
                _evt.Raise();
            }
        }

        public void Raise(T0 arg0, T1 arg1, T2 arg2)
        {
            if (RuntimeApplication.isPlaying)
            {
                _evt.Raise(arg0, arg1, arg2);
            }
        }

        public void RegisterListener(IGameEvent3Register<T0, T1, T2> listener)
        {
            if (RuntimeApplication.isPlaying)
            {

                if (_evt != null)
                {
                    _evt.RegisterListener(listener);
                }
            }
        }
#if UNITY_EDITOR
        public List<IGameEvent3Register<T0, T1, T2>> GetEventListeners()
        {
            return _evt.GetEventListeners();
        }
#endif
        public void UnregisterListener(IGameEvent3Register<T0, T1, T2> listener)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_evt != null)
                {
                    _evt.UnregisterListener(listener);
                }
            }
        }

        public bool Equals(TEvt other)
        {
            return _evt == other;
        }

        internal void Set(TEvt value)
        {
            _evt = value;
        }
    }
}