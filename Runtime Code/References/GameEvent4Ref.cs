﻿using System;
using System.Collections.Generic;
using com.FDT.Common;
using com.FDT.GameEvents;
using UnityEngine;

namespace com.FDT.GameEvents
{
    [System.Serializable]
    public abstract class GameEvent4Ref<TEvt, T0, T1, T2, T3> : GameEventRefBase, IGameEvent, IEquatable<TEvt>
        where TEvt : GameEvent4<T0, T1, T2, T3>
    {
        [SerializeField] protected TEvt _evt;

        public void Raise()
        {
            if (RuntimeApplication.isPlaying)
            {
                _evt.Raise();
            }
        }

        public void Raise(T0 arg0, T1 arg1, T2 arg2, T3 arg3)
        {
            if (RuntimeApplication.isPlaying)
            {
                _evt.Raise(arg0, arg1, arg2, arg3);
            }
        }

        public void RegisterListener(IGameEvent4Register<T0, T1, T2, T3> listener)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_evt != null)
                {
                    _evt.RegisterListener(listener);
                }
            }
        }
#if UNITY_EDITOR
        public List<IGameEvent4Register<T0, T1, T2, T3>> GetEventListeners()
        {
            return _evt.GetEventListeners();
        }
#endif
        public void UnregisterListener(IGameEvent4Register<T0, T1, T2, T3> listener)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_evt != null)
                {
                    _evt.UnregisterListener(listener);
                }
            }
        }

        public bool Equals(TEvt other)
        {
            return _evt == other;
        }

        internal void Set(TEvt value)
        {
            _evt = value;
        }
    }
}