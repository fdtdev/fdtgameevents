﻿using System;
using System.Collections.Generic;
using com.FDT.GameEvents;
using UnityEngine;

namespace com.FDT.GameEvents
{
    [System.Serializable]
    public abstract class GameEvent2Ref<TEvt, T0, T1> : GameEventRefBase, IGameEvent, IEquatable<TEvt> where TEvt : GameEvent2<T0, T1>
    {
        [SerializeField] protected TEvt _evt;

        public void Raise()
        {
            if (Application.isPlaying)
            {
                _evt.Raise();
            }
        }

        public void Raise(T0 arg0, T1 arg1)
        {
            if (Application.isPlaying)
            {
                _evt.Raise(arg0, arg1);
            }
        }

        public void RegisterListener(IGameEvent2Register<T0, T1> listener)
        {
            if (Application.isPlaying)
            {
                if (_evt != null)
                {
                    _evt.RegisterListener(listener);
                }
            }
        }
#if UNITY_EDITOR
        public List<IGameEvent2Register<T0, T1>> GetEventListeners()
        {
            return _evt.GetEventListeners();
        }
#endif
        public void UnregisterListener(IGameEvent2Register<T0, T1> listener)
        {
            if (Application.isPlaying)
            {
                if (_evt != null)
                {
                    _evt.UnregisterListener(listener);
                }
            }
        }

        public bool Equals(TEvt other)
        {
            return _evt == other;
        }

        internal void Set(TEvt value)
        {
            _evt = value;
        }
    }
}