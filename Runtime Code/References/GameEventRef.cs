﻿using System;
using System.Collections.Generic;
using com.FDT.Common;
using com.FDT.GameEvents;
using UnityEngine;

[System.Serializable]
public class GameEventRef: GameEventRefBase, IGameEvent, IEquatable<GameEvent>
{
    [SerializeField] protected GameEvent _evt;

    public void Raise()
    {
        if (RuntimeApplication.isPlaying)
        {
            _evt.Raise();
        }
    }
   
    public void RegisterListener(IGameEventRegister listener)
    {
        if (RuntimeApplication.isPlaying)
        {
            if (_evt != null)
            {
                _evt.RegisterListener(listener);
            }
        }
    }
#if UNITY_EDITOR
    public List<IGameEventRegister> GetEventListeners()
    {
        return _evt.GetEventListeners();
    }
#endif
    public void UnregisterListener(IGameEventRegister listener)
    {
        if (RuntimeApplication.isPlaying)
        {
            if (_evt != null)
            {
                _evt.UnregisterListener(listener);
            }
        }
    }

    public bool Equals(GameEvent other)
    {
        return _evt == other;
    }

    internal void Set(GameEvent value)
    {
        _evt = value;
    }
}
