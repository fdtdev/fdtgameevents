﻿using System;
using System.Collections.Generic;
using com.FDT.GameEvents;
using UnityEngine;

namespace com.FDT.GameEvents
{
    [System.Serializable]
    public abstract class GameEvent1Ref<TEvt, T0> : GameEventRefBase, IGameEvent, IEquatable<TEvt> where TEvt : GameEvent1<T0>
    {
        [SerializeField] protected TEvt _evt;

        public void Raise()
        {
            if (Application.isPlaying)
            {
                _evt.Raise();
            }
        }

        public void Raise(T0 arg0)
        {
            if (Application.isPlaying)
            {
                _evt.Raise(arg0);
            }
        }
        public void RegisterListener(IGameEvent1Register<T0> listener)
        {
            if (Application.isPlaying)
            {
                if (_evt != null)
                {
                    _evt.RegisterListener(listener);
                }
            }
        }
#if UNITY_EDITOR
        public List<IGameEvent1Register<T0>> GetEventListeners()
        {
            return _evt.GetEventListeners();
        }
#endif
        public void UnregisterListener(IGameEvent1Register<T0> listener)
        {
            if (Application.isPlaying)
            {
                if (_evt != null)
                {
                    _evt.UnregisterListener(listener);
                }
            }
        }

        public bool Equals(TEvt other)
        {
            return _evt == other;
        }

        internal void Set(TEvt value)
        {
            _evt = value;
        }
    }
}
