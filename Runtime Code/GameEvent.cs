﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.GameEvents
{
	[CreateAssetMenu(menuName = "FDT/GameEvents/GameEvent")]
    public class GameEvent : GameEventBase
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
		private readonly List<IGameEventRegister> eventListeners = new List<IGameEventRegister>();

        public override void ResetData()
        {
            base.ResetData();
            eventListeners.Clear();
        }

        public override void Raise()
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> Raise called for <color=green><b>{this.name}</b></color>", this);
            }
            for(int i = eventListeners.Count -1; i >= 0; i--)
                eventListeners[i].OnEventRaised(this);
        }

		public void RegisterListener(IGameEventRegister listener)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> RegisterListener called for <color=green><b>{this.name}</b></color> with listener {(listener is Object?((Object) listener).name:listener as object)}", this);
            }
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }
#if UNITY_EDITOR
        public List<IGameEventRegister> GetEventListeners()
        {
            return eventListeners;
        }
#endif
		public void UnregisterListener(IGameEventRegister listener)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> UnregisterListener called for <color=green><b>{this.name}</b></color> with listener {(listener is Object?((Object) listener).name:listener as object)}", this);
            }
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }
    }
}