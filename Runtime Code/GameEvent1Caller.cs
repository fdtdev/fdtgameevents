﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.GameEvents
{
    public class GameEvent1Caller<T0, TEvt> : GameEventCallerBase where TEvt:GameEventBase
    {
        #region Fields
        [SerializeField] protected TEvt _Event;
        [Space] public T0 arg0;
        #endregion

        #region Methods

        public void Raise()
        {
            if (RuntimeApplication.isPlaying)
            {
                (_Event as GameEvent1<T0>).Raise(arg0);
            }
        }
        #endregion
    }
}