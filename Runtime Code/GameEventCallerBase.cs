﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace com.FDT.GameEvents
{
    public class GameEventCallerBase : MonoBehaviour
    {
        [TextArea] public string description;
    }
}