﻿using com.FDT.Common;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.GameEvents
{
    public class GameEvent1Listener<T0, TEvt, TUEvt> : GameEventListenerBase, IGameEvent1Register<T0>
        where TUEvt : UnityEvent<T0>
        where TEvt:GameEventBase
    {
        [Tooltip("Event to register with.")] public TEvt Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public TUEvt Response;

        public System.Action<T0> GetExecute()
        {
            return null;
        }

        private void OnEnable()
        {
            if (RuntimeApplication.isPlaying)
            {
                (Event as GameEvent1<T0>).RegisterListener(this);
            }
        }

        private void OnDisable()
        {
            if (RuntimeApplication.isPlaying)
            {
                (Event as GameEvent1<T0>).UnregisterListener(this);
            }
        }

        public void OnEventRaised(GameEvent1<T0> evt, T0 arg0)
        {
            Response.Invoke(arg0);
        }
   }
}