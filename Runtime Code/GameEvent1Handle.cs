﻿using System;
using com.FDT.Common;
using UnityEngine;

namespace com.FDT.GameEvents
{
    [System.Serializable]
    public abstract class GameEvent1Handle<T0, TEvt> : GameEventHandleBase, IGameEvent1Register<T0>
        where TEvt:GameEventBase
    {
#if UNITY_EDITOR
        public Action<T0> GetExecute()
        {
            return _Execute;
        }

        public GameObject GetGameObject()
        {
            return null;
        }
#endif

        [SerializeField] protected TEvt _Event;

        protected Action<T0> _Execute;

        public void OnEventRaised(GameEvent1<T0> evt, T0 arg0)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_Execute != null)
                    _Execute(arg0);
            }
        }

        public void AddEventListener(Action<T0> action)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (listeners == 0)
                    Init();
                _Execute += action;
                listeners++;
            }
        }

        public void RemoveEventListener(Action<T0> action)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (CanRemoveListener())
                {
                    _Execute -= action;
                }
            }
        }

        protected bool CanRemoveListener()
        {
            if (RuntimeApplication.isPlaying)
            {
                if (listeners > 0)
                {
                    listeners--;
                    if (listeners == 0)
                        Dispose();
                }

                return true;
            }
            else
            {
                listeners = 0;
                _Execute = null;
                return false;
            }
        }

        protected void Init()
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_Event != null)
                    (_Event as GameEvent1<T0>).RegisterListener(this);
            }
        }

        protected void Dispose()
        {
            if (RuntimeApplication.isPlaying)
            {
                listeners = 0;
                if (_Event != null)
                    (_Event as GameEvent1<T0>).UnregisterListener(this);
            }
        }

        public GameEvent1<T0> Event
        {
            set
            {
                Dispose();
                _Event = (TEvt) (object) value;
                Init();
            }
        }
    }
}