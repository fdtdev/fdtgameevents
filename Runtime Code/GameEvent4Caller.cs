﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.GameEvents
{
    public class GameEvent4Caller<T0, T1, T2, T3, TEvt> : GameEventCallerBase where TEvt:GameEventBase
    {
        #region Fields
        [SerializeField] protected TEvt _Event;
        [Space] public T0 arg0;
        public T1 arg1;
        public T2 arg2;
        public T3 arg3;
        #endregion

        #region Methods

        public void Raise()
        {
            if (RuntimeApplication.isPlaying)
            {
                (_Event as GameEvent4<T0, T1, T2, T3>).Raise(arg0, arg1, arg2, arg3);
            }
        }

        #endregion
    }
}