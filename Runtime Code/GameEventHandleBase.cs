﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace com.FDT.GameEvents
{
    [System.Serializable]
    public abstract class GameEventHandleBase
    {
        protected int listeners = 0;
    }
}