﻿using com.FDT.Common;

namespace com.FDT.GameEvents
{
    public abstract class GameEventBase : IDAsset, IGameEvent
    {
        public virtual void Raise()
        {
            throw new System.NotImplementedException();
        }
        public override bool HasCustomIcon => true;
        public override string IconFileName => "GameEvent Icon";
        public bool showDebugLogs = false;
    }
}