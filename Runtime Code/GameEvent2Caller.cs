﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.GameEvents
{
    public class GameEvent2Caller<T0, T1, TEvt> : GameEventCallerBase where TEvt:GameEventBase
    {
        #region Fields

        [SerializeField] protected TEvt _Event;
        [Space] public T0 arg0;
        public T1 arg1;

        #endregion

        #region Methods

        public void Raise()
        {
            if (RuntimeApplication.isPlaying)
            {
                (_Event as GameEvent2<T0, T1>).Raise(arg0, arg1);
            }
        }
        #endregion
    }
}