﻿namespace com.FDT.GameEvents
{
    public interface IGameEvent
    {
        // Use this for initialization
        void Raise();
    }
}