﻿using com.FDT.Common;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.GameEvents
{
    public class GameEvent3Listener<T0, T1, T2, TEvt, TUEvt> : GameEventListenerBase, IGameEvent3Register<T0, T1, T2>
        where TUEvt : UnityEvent<T0, T1, T2>
        where TEvt:GameEventBase
    {
        [Tooltip("Event to register with.")] public TEvt Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public TUEvt Response;

        public System.Action<T0, T1, T2> GetExecute()
        {
            return null;
        }

        private void OnEnable()
        {
            if (RuntimeApplication.isPlaying)
            {
                (Event as GameEvent3<T0, T1, T2>).RegisterListener(this);
            }
        }

        private void OnDisable()
        {
            if (RuntimeApplication.isPlaying)
            {
                (Event as GameEvent3<T0, T1, T2>).UnregisterListener(this);
            }
        }

        public void OnEventRaised(GameEvent3<T0, T1, T2> evt, T0 arg0, T1 arg1, T2 arg2)
        {
            Response.Invoke(arg0, arg1, arg2);
        }
    }
}