﻿using UnityEngine;

namespace com.FDT.GameEvents
{
	public interface IGameEvent2Register<T0, T1>
	{
		void OnEventRaised (GameEvent2<T0, T1> evt, T0 arg0, T1 arg1);
		#if UNITY_EDITOR
		System.Action<T0, T1> GetExecute();
		GameObject GetGameObject();
		#endif
	}
}