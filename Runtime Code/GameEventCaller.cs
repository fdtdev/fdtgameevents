﻿using System;
using com.FDT.Common;
using UnityEngine;

namespace com.FDT.GameEvents
{
	public class GameEventCaller : MonoBehaviour {

		#region Fields
		[SerializeField] protected GameEvent _gameEvent;
		#endregion
		#region Methods

        public void Raise()
		{
			if (RuntimeApplication.isPlaying)
			{
				_gameEvent.Raise();
			}
		}

		#endregion
	}
}
