﻿using UnityEngine;

namespace com.FDT.GameEvents
{
    public interface IGameEventRegister
    {
        void OnEventRaised(GameEvent evt);
        #if UNITY_EDITOR
        System.Action GetExecute();
        GameObject GetGameObject();
        #endif
    }
}