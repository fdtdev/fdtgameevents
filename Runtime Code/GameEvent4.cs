﻿using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.GameEvents
{
    public abstract class GameEvent4<T0, T1, T2, T3> : GameEventBase
    {
        private readonly List<IGameEvent4Register<T0, T1, T2, T3>> eventListeners = new List<IGameEvent4Register<T0, T1, T2, T3>>();
        [Space] public T0 arg0;
        public T1 arg1;
        public T2 arg2;
        public T3 arg3;

#if UNITY_EDITOR
        public override void ResetData()
        {
            base.ResetData();
            eventListeners.Clear();
        }    
#endif
        public override void Raise()
        {
            Raise(arg0, arg1, arg2, arg3);
        }
        public void Raise(T0 arg0, T1 arg1, T2 arg2, T3 arg3)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> Raise called for <color=green><b>{this.name}</b></color> with parameters <color=#add8e6ff>{arg0}</color>, <color=teal>{arg1}</color>, <color=#add8e6ff>{arg2}</color>, <color=teal>{arg3}</color>", this);
            }
            for(int _idx = eventListeners.Count -1; _idx >= 0; _idx--)
                eventListeners[_idx].OnEventRaised(this, arg0, arg1, arg2, arg3);
        }
        public void RegisterListener(IGameEvent4Register<T0, T1, T2, T3> listener)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> RegisterListener called for <color=green><b>{this.name}</b></color> with listener {(listener is Object?((Object) listener).name:listener as object)}", this);
            }
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }


        public virtual string arg0label
        {
            get { return null; }
        }
        public virtual string arg1label
        {
            get { return null; }
        }
        public virtual string arg2label
        {
            get { return null; }
        }
        public virtual string arg3label
        {
            get { return null; }
        }
        public List<IGameEvent4Register<T0, T1, T2, T3>> GetEventListeners()
        {
            return eventListeners;
        }
        public void UnregisterListener(IGameEvent4Register<T0, T1, T2, T3> listener)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> UnregisterListener called for <color=green><b>{this.name}</b></color> with listener {(listener is Object?((Object) listener).name:listener as object)}", this);
            }
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }
    }
}