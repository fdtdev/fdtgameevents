﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using com.FDT.Common;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.GameEvents
{
	public class GameEventListener : GameEventListenerBase, IGameEventRegister
    {
        [Tooltip("Event to register with.")]
        public GameEvent Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public UnityEvent Response;

        public System.Action GetExecute()
        {
            return null;
        }
        
        private void OnEnable()
        {
            if (RuntimeApplication.isPlaying)
            {
                Event.RegisterListener(this);
            }
        }

        private void OnDisable()
        {
            if (RuntimeApplication.isPlaying)
            {
                Event.UnregisterListener(this);
            }
        }

        public void OnEventRaised(GameEvent evt)
        {
            Response.Invoke();
        }
    }
}