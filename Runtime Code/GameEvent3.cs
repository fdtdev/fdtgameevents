﻿using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.GameEvents
{
    public abstract class GameEvent3<T0, T1, T2> : GameEventBase
    {
        private readonly List<IGameEvent3Register<T0, T1, T2>> eventListeners = new List<IGameEvent3Register<T0, T1, T2>>();
        [Space] public T0 arg0;
        public T1 arg1;
        public T2 arg2;
        
#if UNITY_EDITOR
        public override void ResetData()
        {
            base.ResetData();
            eventListeners.Clear();
        }    
#endif
        public override void Raise()
        {
            Raise(arg0, arg1, arg2);
        }
        public void Raise(T0 arg0, T1 arg1, T2 arg2)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> Raise called for <color=green><b>{this.name}</b></color> with parameters <color=#add8e6ff>{arg0}</color>, <color=teal>{arg1}</color>, <color=#add8e6ff>{arg2}</color>", this);
            }
            for(int _idx = eventListeners.Count -1; _idx >= 0; _idx--)
                eventListeners[_idx].OnEventRaised(this, arg0, arg1, arg2);
        }
        public void RegisterListener(IGameEvent3Register<T0, T1, T2> listener)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> RegisterListener called for <color=green><b>{this.name}</b></color> with listener {(listener is Object?((Object) listener).name:listener as object)}", this);
            }
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }


        public virtual string arg0label
        {
            get { return null; }
        }
        public virtual string arg1label
        {
            get { return null; }
        }
        public virtual string arg2label
        {
            get { return null; }
        }
        public List<IGameEvent3Register<T0, T1, T2>> GetEventListeners()
        {
            return eventListeners;
        }
        public void UnregisterListener(IGameEvent3Register<T0, T1, T2> listener)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> UnregisterListener called for <color=green><b>{this.name}</b></color> with listener {(listener is Object?((Object) listener).name:listener as object)}", this);
            }
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }
    }
}