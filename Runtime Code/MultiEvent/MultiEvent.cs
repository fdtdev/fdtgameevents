﻿using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.GameEvents
{
    [System.Serializable]
    public class MultiEvent
    {
        public enum MultiEventType
        {
            UNITY_EVENT = 0, GAME_EVENT = 1
        }

        [SerializeField] protected MultiEventType _type;
        public MultiEventType Type { get { return _type; } }

        [SerializeField] protected UnityEvent _uevent;
        public UnityEvent Uevent { get { return _uevent; } }

        [SerializeField] protected GameEventBase _gevent;
        public GameEventBase Gevent { get { return _gevent; } }

        #region Methods
        public void Execute()
        {
            if (_type == MultiEventType.GAME_EVENT && _gevent != null)
                _gevent.Raise();
            else if (_type == MultiEventType.UNITY_EVENT)
                _uevent.Invoke();
        }
        #endregion
    }
}