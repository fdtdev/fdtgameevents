﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEditor;
using UnityEngine;

namespace com.FDT.GameEvents.Editor
{
    [CustomEditor(typeof(GameEvent))]
    public class GameEventEditor : GameEventEditorBase
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawDefaultInspector();
            GameEvent _cTarget = target as GameEvent;
            GUI.enabled = Application.isPlaying;

            if (GUILayout.Button("Raise"))
                _cTarget.Raise();
            if (Application.isPlaying) {
                EditorGUILayout.Space ();
                EditorGUILayout.LabelField ("Listeners");
                var list = (target as GameEvent).GetEventListeners();
                foreach (var l in list) {
                    var e = l.GetExecute ();
                    if (e != null) {
                        DrawExecute(e.Target);
                    } else {
                        GameObject tgo = l.GetGameObject ();
                        if (tgo != null) {
                            DrawGameObject(tgo);
                        }
                    }
                }
            }
            GUI.enabled = true;
            base.OnInspectorGUI();
            serializedObject.ApplyModifiedProperties();
        }
    }
    
}