﻿using UnityEngine;
using UnityEditor;

namespace com.FDT.GameEvents.Editor
{
    [CustomPropertyDrawer(typeof(GameEventRefBase), true)]
    public class GameEventRefBaseDrawer : PropertyDrawer 
    {
        public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty (position, label, property);
            EditorGUI.PropertyField (position, property.FindPropertyRelative("_evt"), label, true);
            EditorGUI.EndProperty ();
        }
    }
}
