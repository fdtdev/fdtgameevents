using UnityEngine;
using UnityEditor;

namespace com.FDT.GameEvents.Editor
{
	public class GameEvent3Editor<T0, T1, T2> : GameEventEditorBase
	{
        protected string[] excluded = new string[] {"arg0", "arg1", "arg2"};
		public override void OnInspectorGUI()
		{
			serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, excluded);

			GameEvent3<T0, T1, T2> _cTarget = target as GameEvent3<T0, T1, T2>;
            
			var arg0prop = serializedObject.FindProperty("arg0");
			string arg0lbl = string.IsNullOrEmpty(_cTarget.arg0label)?"arg0":_cTarget.arg0label;
			if (arg0prop != null)
			{
				EditorGUILayout.PropertyField(arg0prop, new GUIContent(arg0lbl), true);
			}
			else
			{
				EditorGUILayout.LabelField(arg0lbl + ": NonSerialized");
			}
            
			var arg1prop = serializedObject.FindProperty("arg1");
			string arg1lbl = string.IsNullOrEmpty(_cTarget.arg1label)?"arg1":_cTarget.arg1label;
			if (arg1prop != null)
			{
				EditorGUILayout.PropertyField(arg1prop, new GUIContent(arg1lbl), true);
			}
			else
			{
				EditorGUILayout.LabelField(arg1lbl + ": NonSerialized");
			}
            
			var arg2prop = serializedObject.FindProperty("arg2");
			string arg2lbl = string.IsNullOrEmpty(_cTarget.arg2label)?"arg2":_cTarget.arg2label;
			if (arg2prop != null)
			{
				EditorGUILayout.PropertyField(arg2prop, new GUIContent(arg2lbl), true);
			}
			else
			{
				EditorGUILayout.LabelField(arg2lbl + ": NonSerialized");
			}
            
			GUI.enabled = Application.isPlaying;

			if (GUILayout.Button("Raise"))
				_cTarget.Raise();
			if (Application.isPlaying) {
				EditorGUILayout.Space ();
				EditorGUILayout.LabelField ("Listeners");
				var list = (target as GameEvent3<T0, T1, T2>).GetEventListeners();
				foreach (var l in list) {
					var e = l.GetExecute ();
					if (e != null) {
						DrawExecute(e.Target);
					} else {
						GameObject tgo = l.GetGameObject ();
						if (tgo != null) {
							DrawGameObject(tgo);
						}
					}
				}
			}
            GUI.enabled = true;
            base.OnInspectorGUI();
            serializedObject.ApplyModifiedProperties();
        }
	}
}
