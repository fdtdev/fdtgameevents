using UnityEngine;
using UnityEditor;
using com.FDT.GameEvents;

namespace com.FDT.GameEvents.Editor
{
    [CustomPropertyDrawer(typeof(MultiEvent), true)]
    public class MultiEventDrawer : PropertyDrawer
    {

        private SerializedProperty _typeProp;
        private MultiEvent.MultiEventType t;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            bool mini = position.width < 250 ? true : false;
            GetProps(property);
            Rect btnTypeRect = new Rect(position.x, position.y, mini ? 55f : 70f, 16);

            EditorGUI.BeginProperty(position, label, property);
            if (t == MultiEvent.MultiEventType.GAME_EVENT)
            {
                if (GUI.Button(btnTypeRect, mini ? "GameEvt" : "GameEvent", EditorStyles.miniButton))
                {
                    _typeProp.enumValueIndex = (int)MultiEvent.MultiEventType.UNITY_EVENT;
                }
            }
            else
            {
                if (GUI.Button(btnTypeRect, mini ? "UnityEvt" : "UnityEvent", EditorStyles.miniButton))
                {
                    _typeProp.enumValueIndex = (int)MultiEvent.MultiEventType.GAME_EVENT;
                }
            }
            float h = 0;

            if (t == MultiEvent.MultiEventType.UNITY_EVENT)
            {
                SerializedProperty uEventProp = property.FindPropertyRelative("_uevent");
                h = EditorGUI.GetPropertyHeight(uEventProp) + 2;
                Rect unityEvtRect = new Rect(position.x + btnTypeRect.width + 3, position.y, position.width - btnTypeRect.width - 3, position.height);
                GUI.Box(unityEvtRect, GUIContent.none);
                EditorGUI.PropertyField(new Rect(unityEvtRect.x+2, unityEvtRect.y+2, unityEvtRect.width - 4, unityEvtRect.height - 4), uEventProp);
            }
            else if (t == MultiEvent.MultiEventType.GAME_EVENT)
            {
                h = 20;
                Rect gEvtRect = new Rect(position.x + btnTypeRect.width + 3, position.y, position.width - btnTypeRect.width - 6, 16);
                EditorGUI.PropertyField(gEvtRect, property.FindPropertyRelative("_gevent"), GUIContent.none);
            }

            EditorGUI.EndProperty();
        }
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            GetProps(property);
            if (t == MultiEvent.MultiEventType.UNITY_EVENT)
            {
                SerializedProperty uEventProp = property.FindPropertyRelative("_uevent");
                float h = EditorGUI.GetPropertyHeight(uEventProp);
                return 5 + h;
            }
            else if (t == MultiEvent.MultiEventType.GAME_EVENT)
            {
                return 16;
            }
            return 70;
        }
        protected void GetProps(SerializedProperty property)
        {
            _typeProp = property.FindPropertyRelative("_type");
            t = (MultiEvent.MultiEventType)_typeProp.enumValueIndex;
        }
    }
}