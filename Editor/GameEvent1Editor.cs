using UnityEngine;
using UnityEditor;

namespace com.FDT.GameEvents.Editor
{
	public class GameEvent1Editor<T0> : GameEventEditorBase
    {
        protected string[] excluded = new string[] {"arg0"};
		public override void OnInspectorGUI()
		{
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, excluded);

            GameEvent1<T0> _cTarget = target as GameEvent1<T0>;
            var arg0prop = serializedObject.FindProperty("arg0");
            string arg0lbl = string.IsNullOrEmpty(_cTarget.arg0label)?"arg0":_cTarget.arg0label;
            if (arg0prop != null)
            {
	            EditorGUILayout.PropertyField(arg0prop, new GUIContent(arg0lbl), true);
            }
            else
            {
	            EditorGUILayout.LabelField(arg0lbl + ": NonSerialized");
            }
			
			GUI.enabled = Application.isPlaying;

			if (GUILayout.Button("Raise"))
				_cTarget.Raise();
			if (Application.isPlaying) {
				EditorGUILayout.Space ();
				EditorGUILayout.LabelField ("Listeners");
				var list = (target as GameEvent1<T0>).GetEventListeners();
				foreach (var l in list) {
					var e = l.GetExecute ();
					if (e != null) {
						DrawExecute(e.Target);
					} else {
						GameObject tgo = l.GetGameObject ();
						if (tgo != null) {
							DrawGameObject(tgo);
						}
					}
				}
			}
            GUI.enabled = true;
            base.OnInspectorGUI();
            serializedObject.ApplyModifiedProperties();
		}
	}
}
