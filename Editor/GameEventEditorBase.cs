﻿using com.FDT.Common;
using com.FDT.Common.Editor;
using UnityEngine;
using UnityEditor;

namespace com.FDT.GameEvents.Editor
{
    public class GameEventEditorBase : UnityEditor.Editor
    {
        protected void DrawGameObject(GameObject tgo)
        {
            if (tgo != null) {
                EditorGUILayout.ObjectField (tgo, typeof(GameObject), true);
            }
        }

        protected void DrawExecute(object eTarget)
        {
            if (eTarget is MonoBehaviour)
            {
                EditorGUILayout.ObjectField((eTarget as MonoBehaviour), typeof(MonoBehaviour),
                    true);
            }
            else if (eTarget is ScriptableObject)
            {
                EditorGUILayout.ObjectField((eTarget as ScriptableObject), typeof(ScriptableObject),
                    true);
            }
        }

        public override void OnInspectorGUI()
        {
            ScriptableObjectWithDescription cTarget = target as ScriptableObjectWithDescription;
            if (cTarget.HasCustomIcon && GUILayout.Button("Fix Icon"))
            {
                string scriptName = target.GetType().Name;
                string iconFileName = cTarget.IconFileName;
                EditorExtensions.SetIcon(scriptName, iconFileName);
            }
        }
    }
}
