using UnityEngine;
using UnityEditor;

namespace com.FDT.GameEvents.Editor
{
	public class GameEvent2Editor<T0, T1> : GameEventEditorBase
	{
        protected string[] excluded = new string[] {"arg0", "arg1"};
		public override void OnInspectorGUI()
		{
			serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, excluded);

			GameEvent2<T0, T1> _cTarget = target as GameEvent2<T0, T1>;
            
            var arg0prop = serializedObject.FindProperty("arg0");
            string arg0lbl = string.IsNullOrEmpty(_cTarget.arg0label)?"arg0":_cTarget.arg0label;
            if (arg0prop != null)
            {
	            EditorGUILayout.PropertyField(arg0prop, new GUIContent(arg0lbl), true);
            }
            else
            {
	            EditorGUILayout.LabelField(arg0lbl + ": NonSerialized");
            }
            
            var arg1prop = serializedObject.FindProperty("arg1");
            string arg1lbl = string.IsNullOrEmpty(_cTarget.arg1label)?"arg1":_cTarget.arg1label;
            if (arg1prop != null)
            {
	            EditorGUILayout.PropertyField(arg1prop, new GUIContent(arg1lbl), true);
            }
            else
            {
	            EditorGUILayout.LabelField(arg1lbl + ": NonSerialized");
            }
            
			GUI.enabled = Application.isPlaying;

			if (GUILayout.Button("Raise"))
				_cTarget.Raise();
			if (Application.isPlaying) {
				EditorGUILayout.Space ();
				EditorGUILayout.LabelField ("Listeners");
				var list = (target as GameEvent2<T0, T1>).GetEventListeners();
				foreach (var l in list) {
					var e = l.GetExecute ();
					if (e != null) {
						DrawExecute(e.Target);
					} else {
						GameObject tgo = l.GetGameObject ();
						if (tgo != null) {
							DrawGameObject(tgo);
						}
					}
				}
			}
            GUI.enabled = true;
            base.OnInspectorGUI();
            serializedObject.ApplyModifiedProperties();
		}
	}
}
