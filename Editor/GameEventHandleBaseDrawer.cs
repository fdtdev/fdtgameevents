﻿using UnityEditor;
using UnityEngine;

namespace com.FDT.GameEvents.Editor
{
    [CustomPropertyDrawer(typeof(GameEventHandleBase), true)]
    public class GameEventHandleBaseDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty (position, label, property);
            EditorGUI.PropertyField (position, property.FindPropertyRelative("_Event"), label, true);
            EditorGUI.EndProperty ();
        }
    }
}