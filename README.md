# FDT GameEvents

GameEvents is a scriptableObject generic event system based on "the talk". It includes an Editor Window that creates the boilerplate scripts necesary for custom generic GameEvent usage.


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.gameevents": "https://bitbucket.org/fdtdev/fdtgameevents.git#2.3.0",
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.2.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtgameevents/src/2.3.0/LICENSE.md)